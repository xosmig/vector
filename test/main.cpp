
#include "../source/vector_common.hpp"
#include "../source/vector_future.hpp"
#include "../source/vector_past.hpp"

#include "test_manual.hpp"
#include "test_auto.hpp"

#include <vector>
#include <deque>
#include <iostream>
#include <fstream>

template<class T>
using std_vector = std::vector<T, std::allocator<T>>; // my pattern of an abstract vector is: "template<class> class vector"

template<class T>
using std_deque = std::deque<T, std::allocator<T>>; // my pattern of an abstract vector is: "template<class> class vector"

int main() {
    test_auto<std_vector>::test_all("std_vector.log");
    test_auto<std_deque>::test_all("std_deque.log");
    test_auto<my::vector_common>::test_all("my_vector_common.log");
    test_auto<my::vector_future>::test_all("my_vector_future.log");
    test_auto<my::vector_past>::test_all("my_vector_past.log");
}
