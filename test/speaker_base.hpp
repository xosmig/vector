
/**
Doesn't display any information.
*/

#ifndef _SPEAKER_BASE_HPP_
#define _SPEAKER_BASE_HPP_

#include <ostream>
#include <string>

namespace my {
	class speaker_base {
	protected:
		static std::ostream *out; // output stream for all speakers
	public:
		static std::string default_name;

		static void set_output(std::ostream &output_stream) {
			out = &output_stream;
		}

		static std::ostream &output() {
			return *out;
		}

		std::string name;
	
		speaker_base(): name(default_name) {}; // empty constructor
		
		explicit speaker_base(const std::string &_name): name(_name) {} // constructor by name

		speaker_base(const speaker_base &other): name(other.name) {} // copy constructor

		speaker_base(speaker_base &&other): name(std::move(other.name)) {} // move constructor

		speaker_base &operator=(const speaker_base &other) { // copy=
			name = other.name;
			return *this;
		}

		speaker_base &operator=(speaker_base &&other) { // move=
			name = std::move(other.name);
			return *this;
		}

		~speaker_base() {} // destructor
	};
}

#endif // _SPEAKER_BASE_HPP_
