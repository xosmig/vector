
/**
Displays it's name in destructor.
It is necessary to display something at the end of scope.
Fully defined in .h file.
*/

#ifndef _SPEAKER_ENDER_HPP_
#define _SPEAKER_ENDER_HPP_

#include "speaker_base.hpp"

namespace my {
	class speaker_ender : public speaker_base {
	public:
		using speaker_base::speaker_base;
		using speaker_base::operator=;
		~speaker_ender() {
			(*out) << name << std::endl;
		}
	};
}

#endif // _SPEAKER_ENDER_HPP_
