
/**
Count all constructor and destructor calls.
*/

#ifndef _COUNTER_HPP_
#define _COUNTER_HPP_

#include "../defines.hpp"

#include <istream>
#include <ostream>
#include <string>
#include <cstdint>

namespace my {
	template<class T>
	class counter {
	protected:
		static uint32_t _c_empty;
		static uint32_t _c_data;
		static uint32_t _c_copy;
		static uint32_t _c_move;
		static uint32_t _destr;
	public:
		static T default_data;

		static void reset() {
			_c_empty = 0;
			_c_data = 0;
			_c_copy = 0;
			_c_move = 0;
			_destr = 0;
		}

		static uint32_t c_all() { return _c_empty + _c_data + _c_copy + _c_move; }
		static uint32_t c_empty() { return _c_empty; }
		static uint32_t c_data() { return _c_data; }
		static uint32_t c_copy() { return _c_copy; }
		static uint32_t c_move() { return _c_move; }
		static uint32_t destr() { return _destr; }

		static void print(osref out) {
			out << "c_empty: " << _c_empty << ENDL;
			out << "c_data: " << _c_data << ENDL;
			out << "c_copy: " << _c_copy << ENDL;
			out << "c_move: " << _c_move << ENDL;
			out << "destr: " << _destr << ENDL;
			out << "totaly " << c_all() << " constructors and " << _destr << " destructors." << std::endl;
		}

		T data;
	
		counter() noexcept : data(default_data) { ++_c_empty; };
		explicit counter(const T &_data) noexcept : data(_data) { ++_c_data; }
		counter(const counter &other) noexcept : data(other.data) { ++_c_copy; }
		counter(counter &&other) noexcept : data(std::move(other.data)) { ++_c_move; }

		counter<T> &operator=(const counter<T> &other) {
			data = other.data;
			return *this;
		}

		~counter() { ++_destr; }
	};

	template<class data_t>
	uint32_t counter<data_t>::_c_empty = 0;

	template<class data_t>
	uint32_t counter<data_t>::_c_data = 0;

	template<class data_t>
	uint32_t counter<data_t>::_c_copy = 0;

	template<class data_t>
	uint32_t counter<data_t>::_c_move = 0;

	template<class data_t>
	uint32_t counter<data_t>::_destr = 0;

	template<class data_t>
	data_t counter<data_t>::default_data = data_t();

	template<class T>
	std::istream &operator>>(std::istream &in, counter<T> &obj) {
		in >> obj.data;
		return in;
	}

	template<class T>
	std::ostream &operator<<(std::ostream &out, const counter<T> &obj) {
		out << obj.data;
		return out;
	}
}

#endif // _COUNTER_HPP_
