
/**
...
*/

#ifndef _TEST_AUTO_HPP_
#define _TEST_AUTO_HPP_

#include "counter.hpp"
#include "ta_base.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <deque>

template<template<class T> class vector>
class test_auto : public ta_base {
private:
    test_auto() {}

    template<class T>
    static void test_one_typed(isref in, cstrref answer, osref log = std::cerr);

    template<class T>
    static void exec_one_typed(isref in = std::cin, osref out = std::cout, std::vector<op_info> *result = nullptr);
public:
    static void test_all(cstrref log_filename);

    static void test_all(osref log = std::cerr);

    static void test_by_name(cstrref test_name, osref log = std::cerr);

    static void test_one(cstrref test_path, cstrref ans_path, osref log = std::cerr);
    
    static void test_one(isref in, cstrref answer, osref log = std::cerr);

    static void exec_one(isref in = std::cin, osref out = std::cout, std::vector<op_info> *result = nullptr);
};

template<template<class> class vector>
void test_auto<vector>::test_all(cstrref log_filename) {
    std::ofstream log(log_filename);
    test_all(log);
}

template<template<class> class vector>
void test_auto<vector>::test_all(osref log) {
    ifstream list_in(list_file);
    string s;
    while (std::getline(list_in, s)) {
        test_by_name(s, log);
    }
}

template<template<class> class vector>
void test_auto<vector>::test_by_name(cstrref test_name, osref log) {
    log << "test " << test_name << std::endl;
    test_one(get_test_path(test_name), get_ans_path(test_name), log);
}

template<template<class> class vector>
void test_auto<vector>::test_one(cstrref test_path, cstrref ans_path, osref log) {
    ifstream in(ans_path);
    if (in.fail()) {
        log << "can't open " << ans_path << std::endl;
        throw ans_file_problem();
    }
    string answer;
    std::getline(in, answer, '\0');
    if (in.fail() || !in.eof()) {
        log << "can't read " << ans_path << " normally " << std::endl;
        throw ans_file_read_problem();
    }

    in.close();
    in.open(test_path);
    if (in.fail()) {
        log << "can't open " << test_path << std::endl;
        throw test_file_problem();
    }
    
    test_one(in, answer, log);
}

template<template<class> class vector>
void test_auto<vector>::test_one(isref in, cstrref answer, osref log) {
    char type;
    in >> type;
    switch (type) {
    case type_char<int32_t>():
        test_one_typed<int32_t>(in, answer, log);
        break;
    case type_char<int64_t>():
        test_one_typed<int64_t>(in, answer, log);
        break;
    case type_char<string>():
        test_one_typed<string>(in, answer, log);
        break;
    default:
        log << "unknown type character : " << type << std::endl;
        throw wrong_data_type("test_auto::test_one");
    }
}


template<template<class> class vector>
void test_auto<vector>::exec_one(isref in, osref out, std::vector<op_info> *result) {
    char type;
    in >> type;
    switch (type) {
    case type_char<int32_t>():
        exec_one_typed<int32_t>(in, out, result);
        break;
    case type_char<int64_t>():
        exec_one_typed<int64_t>(in, out, result);
        break;
    case type_char<string>():
        exec_one_typed<string>(in, out, result);
        break;
    default:
        std::stringstream ss;
        ss << "test_auto::exec_one : Unknown data type : " << static_cast<int>(type) << ' ' << type;
        throw wrong_data_type(ss.str());
    }
}

template<template<class> class vector>
template<class T>
void test_auto<vector>::test_one_typed(isref in, cstrref answer, osref log) {
    typedef my::counter<T> cnt;
    using namespace std::chrono;

    cnt::reset();

    std::stringstream ans_stream;

    std::vector<op_info> res;

    log << "running..." << std::endl;
    exec_one_typed<cnt>(in, ans_stream, &res);
    
    if (ans_stream.str() != answer) {
        throw wrong_answer();
    }

    if (cnt::c_all() != cnt::destr()) {
        cnt::print(log);
        throw wrong_destructor_count();
    }

    std::array<size_t, op_type::COUNT> added; // (type) -> (count of added elements)
    std::fill(added.begin(), added.end(), 0);
    size_t count_ops = 0;

    op_info::duration sum_time = op_info::duration::zero();
    op_info::duration::rep max_mean_pack_time = 0;

    for (cur : res) {
        added[cur.type] += std::max(static_cast<ptrdiff_t>(0), cur.added);
        sum_time += cur.time;
        count_ops += cur.count;

        op_info::duration::rep mean_pack_time = duration_cast<nanoseconds>(cur.time).count() / cur.count;
        if (max_mean_pack_time < mean_pack_time) {
            // log << endl << mean_pack_time << ' ' << &cur - res.data() << endl << endl;
            max_mean_pack_time = mean_pack_time;
        }
    }

    // behaviour of std::vector ans std::deque is too complicated and doesn't require checking
    if (! std::is_same< vector<int>, std::vector<int> >() && ! std::is_same< vector<int>, std::deque<int> >()) { 
        auto check = [](osref _log, cstrref c_name, size_t real, size_t wanted) {
            if (real != wanted) {
                cnt::print(_log);
                throw wrong_constructor_count(
                    "wrong " 
                    + c_name
                    + " constructors number. Wanted : " 
                    + std::to_string(wanted)
                    + " received: "
                    + std::to_string(real)
                );
            }
        };

        check(log, "copy", cnt::c_copy(), added[op_type::push_back] + added[op_type::resize_copy] + added[op_type::assign]);
        check(log, "empty", cnt::c_empty(), added[op_type::emplace_back] + added[op_type::resize_empty] + 1 /*reader*/);
    }

    log << "OK:\n";
    log << "sum_time: " << duration_cast<nanoseconds>(sum_time).count() << " nanoseconds\n";
    log << "mean time: " << duration_cast<nanoseconds>(sum_time).count() / count_ops << " nanoseconds\n";
    log << "max mean pack time: " << max_mean_pack_time << " nanoseconds\n";
    log << endl;
}

template<template<class> class vector>
template<class T>
void test_auto<vector>::exec_one_typed(isref in, osref out, std::vector<op_info> *result) {
    using namespace std::chrono;

    char ch = in.peek();
    if (!isdigit(ch)) {
        in.ignore();
    }

    vector<T> v;

    T reader;

    uint16f type_int;
    while (in >> type_int) { // without any input checking for speed
        if (!(type_int > op_type::unknown && type_int < op_type::COUNT)) {
            throw wrong_operation_type("test_auto::exec_one_typed : Unknown operation type id : " + std::to_string(type_int));
        }

        op_type::obj type = static_cast<op_type::obj>(type_int);

        size_t num, cnt;

        #define __record(cnt, _operation) { \
            if (result) { \
                ptrdiff_t size_begin = v.size(); \
                op_info::duration time; \
                if (cnt == 1) { \
                    auto begin = high_resolution_clock::now(); \
                    _operation; \
                    time = high_resolution_clock::now() - begin; \
                } else { \
                    auto begin = high_resolution_clock::now(); \
                    for (size_t i = 0; i < cnt; ++i) { \
                        _operation; \
                    } \
                    time = high_resolution_clock::now() - begin; \
                } \
                result->emplace_back(type, time, static_cast<ptrdiff_t>(v.size()) - size_begin, cnt); \
            } else { \
                for (size_t i = 0; i < cnt; ++i) { \
                    _operation; \
                } \
            } \
        }

        // inform the compiller that variable is used
        #define __touch(obj) ((void)(obj)) 

        switch (type) {
        case op_type::      push_back:
            in >> reader >> cnt;
            __record(cnt, v.push_back(reader));
            break;

        case op_type::      emplace_back:
            in >> cnt;
            __record(cnt, v.emplace_back());
            break;
        
        case op_type::      pop_back:
            in >> cnt;
            __record(cnt, v.pop_back());
            break;
        
/*      case op_type::      reserve: // std::deque hasn't it
            in >> num;
            __record(1, v.reserve(num));
            break;*/
        
        case op_type::      shrink_to_fit:
            __record(1, v.shrink_to_fit());
            break;
        
        case op_type::      resize_empty:
            in >> num;
            __record(1, v.resize(num));
            break;
        
        case op_type::      resize_copy:
            in >> num >> reader;
            __record(1, v.resize(num, reader));
            break;
        
        case op_type::      assign:
            in >> num >> reader;
            __record(1, v.assign(num, reader));
            if (result) {
                result->back().added = num;
            }
            break;

        case op_type::      access:
            __record(1, for (x : v) __touch(x));
            break;

        case op_type::      access_one:
            in >> num >> cnt;
            __record(cnt, __touch(v.operator[](num)));
            break;
        
        case op_type::      print:
            for (x : v) {
                out << x << ' ';
            }
            out << ENDL;
            break;

        case op_type::      print_one:
            in >> num;
            out << v[num] << ENDL;
            break;
                
        case op_type::      get_size:
            in >> cnt;
            __record(cnt, __touch(v.size()));
            break;

        case op_type::      print_size:
            out << v.size() << ENDL;
            break;

        default:
            break;
        }

        #undef __touch
        #undef __record
    }

    out << endl;
}

#endif // _TEST_AUTO_HPP_
