
#include "../../defines.hpp"
#include "../test_auto.hpp"

#include "generators.hpp"

#include <functional>
#include <vector>
#include <fstream>

template<class T> 
using std_vector = std::vector<T, std::allocator<T>>;

static inline void make_answer(cstrref name) {
	std::ifstream in(ta_base::get_test_path(name));
	std::ofstream out(ta_base::get_ans_path(name));
	
	test_auto<std_vector>::exec_one(in, out); // create answer
}

std::ofstream list_out;

template<class Function, class ... Args>
static inline void create_one(cstrref name, Function generator, const Args & ... args) {
	list_out << name << endl;
	generator(ta_base::get_test_path(name), args...); // generate
	make_answer(name);
}

static inline void create_all() {
	create_one("push_1_x_3e6", push<int64_t>, 1, 3e6, 42);
	create_one("push_3e6_x_1", push<int64_t>, 3e6, 1, 42);
	create_one("access_1_x_3e6_size_1e5", access<int64_t>, 1e5, 1, 3e6);
	create_one("access_3e6_x_1_size_1e5", access<int64_t>, 1e5, 3e6, 1);
	create_one("access_all_1e7_x_20", access_all<int64_t>, 1e7, 20);
	create_one("random_1e4", chaos, 1e4);
}

int main() {
	srand(time(0));
	list_out.open(ta_base::list_file);
	create_all();
}
