

#ifndef _GENERATORS_HPP_
#define _GENERATORS_HPP_

// Warning! Need enabled optimization for normal speed.
inline void print_line(osref out) {
    out << ENDL;
}

template<class Head, class ... Tail>
inline void print_line(osref out, const Head &head, const Tail & ... tail) {
    out << head << ' ';
    print_line(out, tail...);
}

template<class T>
void push(cstrref name, size_t count, size_t block_size, const T &obj) {
    std::ofstream out(name);
    print_line(out, ta_base::type_char<T>());
    while (count != 0) {
        --count;
        print_line(out, ta_base::op_type::push_back, obj, block_size);
    }
}

template<class T>
void access(cstrref name, size_t size, size_t count, size_t block_size) {
    std::ofstream out(name);
    print_line(out, ta_base::type_char<T>());
    print_line(out, ta_base::op_type::resize_empty, size);
    while (count != 0) {
        --count;
        print_line(out, ta_base::op_type::access_one, rand() % size, block_size);
    }
}

// testing for (x : v) speed
template<class T>
void access_all(cstrref name, size_t size, size_t count) {
    std::ofstream out(name);

    print_line(out, ta_base::type_char<T>());
    print_line(out, ta_base::op_type::resize_empty, size);
    while (count != 0) {
        --count;
        print_line(out, ta_base::op_type::access);
    }   
}

// a good test for correctness checking
void chaos(cstrref name, size_t count) {
    std::ofstream out(name);

    static const size_t MAX_SIZE = 1e4;
    size_t size = rand() % MAX_SIZE;

    print_line(out, ta_base::type_char<int64_t>());
    print_line(out, ta_base::op_type::resize_empty, size);

    while (count != 0) {
        --count;
        
        size_t num;

        size_t type;
        if (size == MAX_SIZE) {
            type = rand() % 7 + 2;
        }
        else if (size == 0) {
            type = rand() % 7;
        }
        else {
            type = rand() % 9;
        }

        switch (type) {
        // requires not a full container
        case 0: // 3
            num = rand() % (MAX_SIZE - size) + 1;
            print_line(out, ta_base::op_type::push_back, rand(), num);
            size += num;
            break;
        case 1:
            num = rand() % (MAX_SIZE - size) + 1;
            print_line(out, ta_base::op_type::emplace_back, num);
            size += num;
            break;

        // without additional requirements
        case 2:
            print_line(out, ta_base::op_type::print_size);
            break;
        case 3:
            print_line(out, ta_base::op_type::shrink_to_fit);
            break;
        case 4:
            num = rand() % MAX_SIZE;
            print_line(out, ta_base::op_type::resize_empty, num);
            size = num;
            break;
        case 5:
            num = rand() % MAX_SIZE;
            print_line(out, ta_base::op_type::resize_copy, num, rand());
            size = num;
            break;
        case 6:
            num = rand() % MAX_SIZE;
            print_line(out, ta_base::op_type::assign, num, rand());
            size = num;
            break;

        // requires not an empty container 
        case 7:
            print_line(out, ta_base::op_type::print);
            break;
        case 8:
            num = rand() % size + 1;
            print_line(out, ta_base::op_type::pop_back, num);
            size -= num;
            break;
        }
    }
}

#endif // _GENERATORS_HPP_
