
/**
Displays maximum information.
Fully defined in .h file.
*/

#ifndef _SPEAKER_FULL_HPP_
#define _SPEAKER_FULL_HPP_

#include "speaker_base.hpp"

namespace my {
	class speaker_full : public speaker_base {
	public:
		speaker_full() {
			(*out) << name << " : initialized by default to " << this << std::endl;
		};
		
		explicit speaker_full(const std::string &_name): speaker_base(_name) {
			(*out) << name << " : initialized to " << this << std::endl;
		}

		speaker_full(const speaker_full &other): speaker_base(other) {
			(*out) << name << " : copied from " << &other << " to " << this << std::endl;
		}

		speaker_full(speaker_full &&other): speaker_base(other) {
			(*out) << name << " : moved from " << &other << " to " << this << std::endl;
		}

		speaker_full &operator=(const speaker_full &other) {
			dynamic_cast<speaker_base&>(*this) = other;
			// name = other.name;
			(*out) << name << " : copy operator= from " << &other << " to " << this << std::endl;
			return *this;
		}

		speaker_full &operator=(speaker_full &&other) {
			dynamic_cast<speaker_base&>(*this) = std::forward<speaker_full>(other);
			// name = std::move(other.name);
			(*out) << name << " : move operator= from " << &other << " to " << this << std::endl;
			return *this;
		}

		~speaker_full() {
			(*out) << name << " : deleted from " << this << std::endl;
		}
	};
}

#endif // _SPEAKER_FULL_HPP_
