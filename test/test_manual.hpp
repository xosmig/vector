
/**
Simple tests of base member functions for manual checking.
Necessary to see working with memory.
*/

#ifndef _TEST_MANUAL_HPP_
#define _TEST_MANUAL_HPP_

#include "../defines.hpp"

#include "speaker_full.hpp"
#include "speaker_ender.hpp"

#include <memory>
#include <ostream>
#include <iostream>
#include <fstream>

template<template<class> class vector, class speaker = my::speaker_full>
class test_manual {
public:
	static void test_all(cstrref filename);
	static void test_all(std::ostream &out = std::cerr);

	static void test_constructor(std::ostream &out = std::cerr);
	static void simple_test(std::ostream &out = std::cerr); // possible to check it automaticly	
	static void test_emplace_and_push_back(std::ostream &out = std::cerr);
	static void test_move(std::ostream &out = std::cerr); // possible to check it automaticly
};

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::test_all(cstrref filename) {
	std::ofstream out(filename);
	test_manual<vector, speaker>::test_all(out);
}

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::test_all(std::ostream &out) {
	out << "manual test ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n" << std::endl;

	test_constructor(out);
	simple_test(out);
	test_emplace_and_push_back(out);
	test_move(out);
}

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::test_constructor(std::ostream &out) {
	out << 
	"test constructor & destructor ::::::::::::::::::::::::::::::::::::::::::\n"
	"new speaker Initializer\n"
	"new vector<speaker> v(3, default)\n"
	"5 x v.push_back(Initializer)\n"
	"3 x v.pop_back()\n"
	"end of function (destructs all)\n"
	<< std::endl;
	my::speaker_base::set_output(out);
	
	my::speaker_ender ender("========================================================================\n\n");

	speaker el("Initializer");
	out << std::endl;
	vector<speaker> v(3);
	out << std::endl;
	for (int i = 0; i < 5; ++i) {
		v.push_back(el);
	}
	out << std::endl;
	for (int i = 0; i < 3; ++i) {
		v.pop_back();
	}

	out << "\n======= end of function =======\n" << std::endl;
}

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::simple_test(std::ostream &out) {
	out << 
	"simple test ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
	"new vector<int> v(empty)\n"
	"v.push_back(7)\n"
	"out << v[0]\n"
	"v.reserve(5)\n"
	"v.push_back(2)\n"
	"out << v[0] \n"
	"out << v[1] \n"
	"end of function (destructs all)\n"
	<< std::endl;
	my::speaker_base::set_output(out);

	my::speaker_ender ender("========================================================================\n\n");

	vector<int> v;
	v.push_back(7);
	out << v[0] << std::endl;
	v.reserve(5);
	v.push_back(2);
	out << v[0] << std::endl;
	out << v[1] << std::endl;

	out << "\n======= end of function =======\n" << std::endl;
}

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::test_emplace_and_push_back(std::ostream &out) {
	out << 
	"test emplace_back ::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
	"new vector<speaker> v(2, default)\n"
	"3 x v.push_back(new speaker pushed_element)\n"
	"4 x v.emplace_back(emplaced_element)\n"
	"new speaker for_push\n"
	"2 x v.push_back(for_push)"
	"end of function (destructs all)\n"
	<< std::endl;
	my::speaker_base::set_output(out);

	my::speaker_ender ender("========================================================================\n\n");

	vector<speaker> v(2);
	
	out << std::endl;
	for (int i = 0; i < 3; ++i) {
		v.push_back(speaker("pushed_element"));
	}
	
	out << std::endl;
	for (int i = 0; i < 4; ++ i) {
		v.emplace_back("emplaced_element");
	}

	out << std::endl;
	speaker for_push("for_push");
	for (int i = 0; i < 2; ++i) {
		v.push_back(for_push);
	}
	
	out << "\n======= end of function =======\n" << std::endl;
}

template<template<class> class vector, class speaker>
void test_manual<vector, speaker>::test_move(std::ostream &out) {
	out << 
	"test move ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
	"new vector<int> a(3, 7)\n"
	"new vector<int> b(5, default)\n"
	"b[3] = 17\n"
	"out << a.data() ' ' b.data()\n"
	"out << a.size() ' ' b.size()\n"
	"a = move(b)\n"
	"out << a.data() ' ' b.data()\n"
	"out << a.size() ' ' b.size()\n"
	"end of function (destructs all)\n"
	<< std::endl;
	my::speaker_base::set_output(out);

	my::speaker_ender ender("========================================================================\n\n");

	vector<int> a(3, 7), b(5);
	b[3] = 17;
	out << a.data() << ' ' << b.data() << std::endl;
	out << a.size() << ' ' << b.size() << std::endl;
	a = std::move(b);
	out << a.data() << ' ' << b.data() << std::endl;
	out << a.size() << ' ' << b.size() << std::endl;

	out << "\n======= end of function =======\n" << std::endl;	
}

#endif // _TEST_MANUAL_HPP_
