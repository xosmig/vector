
/**

*/

#ifndef _TA_BASE_H
#define _TA_BASE_H

#include <string>
#include <vector>
#include <chrono>
#include <functional>

#include "../defines.hpp"

class ta_base {
public:
    struct op_type { // it's necessary for imlicit cast op_type to int
        enum obj : uint16f {
            unknown,
            push_back, //(obj x, int cnt): cnt * push_back(x)
            emplace_back, // (int cnt) : cnt * emplace_back()
            pop_back, // (int cnt) : cnt * pop_back()
            // reserve, // (int n) : reserve(n) // std::deque doesn't contains it
            shrink_to_fit, // () : shrink_to_fit()
            resize_empty, // (int n) : resize(n)
            resize_copy, // (int n, obj x) : resize(n, x)
            assign, // (int n, obj x) : assign(n, x)
            access, // () : for (x : v);
            access_one, // (int i, int cnt) : cnt * operator[](i)
            access_const,  // (int i, int cnt) : cnt * operator[](i) const
            print, // () : displays a whole container
            print_one, // (int i) : print(v[i])
            get_size, // (int cnt) : cnt * size()
            print_size, // () : print(size)
            COUNT // == count of objects of this enum
        };
    protected:
        op_type(){} // it's shouldn't be created
    };

    template<class T>
    static constexpr char type_char();

    struct op_info {
        typedef std::chrono::high_resolution_clock::duration duration;

        duration time;
        ptrdiff_t added; // the number of adding elements(may be negative)
        size_t count;
        op_type::obj type;

        op_info(op_type::obj _type, duration _time, ptrdiff_t  _added = 0, uint32f _count = 1): time(_time), added(_added), count(_count), type(_type) {}
    }; 

    static const string tests_dir;
    static const string ans_dir;
    static const string list_file;

    // exceptions
    class exception : public std::exception {
        string reason;
    public:
        explicit exception(const char *_reason = "--- Not stated ---") noexcept: reason(_reason) {}
        explicit exception(cstrref _reason) noexcept: reason(_reason) {}
        const char *what() const noexcept { return reason.c_str(); }
    };

    struct wrong_answer : exception { using exception::exception; };
    struct test_file_problem : exception { using exception::exception; };
    struct ans_file_problem : exception { using exception::exception; };
    struct ans_file_read_problem : exception { using exception::exception; };
    struct wrong_data_type : exception { using exception::exception; };
    struct wrong_operation_type : exception { using exception::exception; };
    struct wrong_query : exception { using exception::exception; };
    struct wrong_destructor_count : exception { using exception::exception; };
    struct wrong_constructor_count : exception { using exception::exception; };

    static std::string get_ans_path(cstrref name) {
        return ans_dir + '/' + name + ".ans";
    }

    static std::string get_test_path(cstrref name) {
        return tests_dir + '/' + name + ".test";
    }
};

template<>
constexpr char ta_base::type_char<int32_t>() {
    return 'i';
} 

template<>
constexpr char ta_base::type_char<int64_t>() {
    return 'l';
}

template<>
constexpr char ta_base::type_char<string>() {
    return 's';
}



#endif // _TA_BASE_H
