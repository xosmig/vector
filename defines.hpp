
#ifndef _DEFINES_HPP_
#define _DEFINES_HPP_

#ifndef _GLIBCXX_DEBUG
#define NDEBUG
#endif
#include <cassert>


#include <string> 
#include <iostream>
#include <fstream>
#include <cstdint>

typedef std::size_t size_t;
typedef std::ptrdiff_t ptrdiff_t;

using std::string;

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

using std::ifstream;
using std::ofstream;

typedef const std::string &cstrref;
typedef std::istream &isref;
typedef std::ostream &osref;

// typedef int_fast8_t int8f; // int8 is char 
typedef int_fast16_t int16f;
typedef int_fast32_t int32f;
typedef int_fast64_t int64f;

// typedef uint_fast8_t uint8f;
typedef uint_fast16_t uint16f;
typedef uint_fast32_t uint32f;
typedef uint_fast64_t uint64f;

const char ENDL = '\n'; // end the line without flushing

#endif // _DEFINES_HPP_
