
/**
_data:          ______xxxxxxxxx_________
_next_data:     xxxxxx_______________________________
                ^     ^        ^        ^            ^
                0   _moved   _size   _capacity   _next_capacity
where:  x - object 
        _ - empty memory
*/

#ifndef _VECTOR_FUTURE_HPP_
#define _VECTOR_FUTURE_HPP_

#include "../defines.hpp"

#include <utility>
#include <memory>

namespace my {

    template<class T>
    class vector_future {
    private:
        static T *allocate(size_t count) noexcept(false) { return static_cast<T*>(operator new(count * sizeof(T))); }
        static void deallocate(T *ptr) noexcept(false) { operator delete(ptr); }

        template<class Val>
        class _iterator : public std::iterator<std::random_access_iterator_tag, Val> {
            friend class vector_future;

            Val *p, *border, *next;

            explicit _iterator(Val *_p, Val *_border, Val *_next) noexcept : p(_p), border(_border), next(_next) {}
        public:
            _iterator(const _iterator &other) noexcept : p(other.p), border(other.border), next(other.next) {}
            bool operator==(_iterator &other) const noexcept { return p == other.p; }
            bool operator!=(_iterator &other) const noexcept { return p != other.p; }
            Val &operator*() const noexcept { return *p; }
            Val *operator->() const noexcept { return p; }
            _iterator &operator++() { 
                ++p; 
                if (p == border) {
                    p = next;
                    --next, --border; 
                    std::swap(next, border); // in order to be able to go back
                }
                return *this;
            }
        };

        size_t next_capacity() noexcept { return 2 * _capacity; }

        T *_data = nullptr, *_next_data = nullptr;
        size_t _size = 0, _moved = 0, _capacity = 0;

        // depends on a move constructor
        void move_one() noexcept(noexcept( T(T()) )); // O(1)

        void resize_down(size_t pos) noexcept; // count <= _size. Destructs element from pos to size - 1 // O(_size - pos)

        void try_move() noexcept(noexcept( move_one )); // O(1)
        void swap_data() noexcept(false); // Allocates new memory. // O(1)

        void try_expand() noexcept(false); // check if it's time to expand

        void reallocate(size_t count) noexcept(false); // makes _capacity = count // O(_size)
    public:
        typedef _iterator<T> iterator;
        // typedef _iterator<const T> const_iterator;

        iterator begin() noexcept { 
            if (_moved == 0) {
                return iterator(_data, nullptr, nullptr);
            } else {
                return iterator(_next_data, _next_data + _moved, _data + _moved); 
            }
        }

        iterator end() noexcept { 
            return iterator(_data + _size, _data + _moved - 1, _next_data + _moved - 1); 
        }

        explicit vector_future(size_t count = 0) noexcept(false) { resize(count); } // O(count)
        explicit vector_future(size_t count, const T &init) noexcept(false) { resize(count, init); } // O(count)
        vector_future(const vector_future &other) noexcept(false); // O(other._size)
        vector_future(vector_future &&other) noexcept; // O(1)

        ~vector_future() noexcept; // O(_size)

        size_t size() const noexcept { return _size; }
        size_t capacity() const noexcept { return _capacity; } // number of elements can be pushed before new allocate of memory

        T & operator[](size_t pos) noexcept; // O(1)
        const T & operator[](size_t pos) const noexcept; // O(1)

        template<class ... args_t>
        void emplace_back(args_t && ... args) noexcept(false); // O(1)
        void push_back(const T &x) noexcept(false) { emplace_back(x); } // O(1)
        void push_back(T &&x) noexcept(false) { emplace_back(std::forward<T>(x)); } // O(1)

        void pop_back() noexcept; // O(1)

        void swap(vector_future &other) noexcept; // O(1)
        vector_future &operator=(vector_future other) noexcept(false); // O(_size) + (copy: O(other._size), move: O(1))
        
        void reserve(size_t count) noexcept(false); //  O(count + _size)
        void shrink_to_fit() noexcept(false) { reallocate(_size); } // O(_size)
        void clear() noexcept { resize_down(0); } // O(_size)

        template<class ... args_t>
        void resize_emplace(size_t count, const args_t & ... args) noexcept(false); // O(count + _size) 
        void resize(size_t count) noexcept(false) { resize_emplace(count); } // O(count + _size) 
        void resize(size_t count, const T &init) noexcept(false) { resize_emplace(count, init); } // O(count + _size) 
        void assign(size_t count, const T &init) noexcept(false); // O(count + _size) 
    };
}

#include "implementations/vector_future_impl.hpp"

#endif // _VECTOR_FUTURE_HPP_
