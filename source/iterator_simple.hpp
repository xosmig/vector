
#ifndef _ITERATOR_SIMPLE_HPP_
#define _ITERATOR_SIMPLE_HPP_

#include <iterator>

namespace my {
    
    template<class Container, class Val>
    class iterator_simple : public std::iterator<std::random_access_iterator_tag, Val> {
        friend Container;

        Val *p;

        explicit iterator_simple(Val *ptr) noexcept : p(ptr) {}
    public:
        iterator_simple(const iterator_simple &other) noexcept: p(other.p) {}
        bool operator==(iterator_simple &other) const noexcept { return p == other.p; }
        bool operator!=(iterator_simple &other) const noexcept { return p != other.p; }
        Val &operator*() const noexcept { return *p; }
        Val *operator->() const noexcept { return p; }
        iterator_simple &operator++() { ++p; return *this; }
    };

}

#endif // _ITERATOR_SIMPLE_HPP_
