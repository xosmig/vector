
/**
_data:          xxxxxx_________            
                ^     ^        ^
                0   _size   _capacity
where:  x - object 
        _ - empty memory
*/

#ifndef _VECTOR_HPP_
#define _VECTOR_HPP_

#include "iterator_simple.hpp"

#include <utility>
#include <memory>

namespace my {

    template<class T>
    class vector_common {
    private:
        static void deallocate(T *ptr) noexcept(false) { operator delete(ptr); }
        static T *allocate(size_t count) noexcept(false) { return static_cast<T*>(operator new(count * sizeof(T))); }

        T *_data = nullptr;
        size_t _size = 0, _capacity = 0;

        void try_expand() noexcept(false); // check if it's time for expand
        void resize_down(size_t pos) noexcept; // count <= _size. Destructs element from pos to size - 1
        void reallocate(size_t count) noexcept(false); // force reallocate for count objects
    public:
        typedef iterator_simple<vector_common, T> iterator;
        // typedef iterator_simple<vector_common, const T> const_iterator;

        iterator begin() const noexcept { return iterator(_data); }
        iterator end() const noexcept { return iterator(_data + _size); }
        
        explicit vector_common(size_t count = 0) noexcept(false) { resize(count); }
        explicit vector_common(size_t count, const T &init) noexcept(false) { resize(count, init); }
        vector_common(const vector_common<T> &other) noexcept(false);
        vector_common(vector_common<T> &&other) noexcept;

        ~vector_common() noexcept;

        size_t size() const noexcept { return _size; }
        size_t capacity() const noexcept { return _capacity; }
        T *data() noexcept { return _data; }
        const T *data() const noexcept { return _data; }

        T & operator[](size_t pos) noexcept { return _data[pos]; }
        const T & operator[](size_t pos) const noexcept { return _data[pos]; }

        void reserve(size_t count) noexcept(false) { if (count > _capacity) reallocate(count); }
        void shrink_to_fit() noexcept(false) { reallocate(_size); };

        template<class ... args_t>
        void resize_emplace(size_t count, const args_t & ... args) noexcept(false);
        void resize(size_t count) noexcept(false) { resize_emplace(count); }
        void resize(size_t count, const T &init) noexcept(false) { resize_emplace(count, init); }
        void assign(size_t count, const T &init) noexcept(false);
        void clear() noexcept { resize_down(0); }

        void swap(vector_common<T> &other) noexcept;
        vector_common<T> &operator=(vector_common<T> other) noexcept(false);

        template<class ... args_t>
        void emplace_back(args_t && ... args) noexcept(false);
        void push_back(const T &x) noexcept(false) { emplace_back(x); }
        void push_back(T &&x) noexcept(false) { emplace_back(std::forward<T>(x)); }

        void pop_back() noexcept;
    };
}

#include "implementations/vector_common_impl.hpp"

#endif // _VECTOR_HPP_
