

#ifndef _VECTOR_PAST_IMPL_HPP_
#define _VECTOR_PAST_IMPL_HPP_

namespace my {
    /// assist

    template<class T>
    void vector_past<T>::move_one() noexcept(noexcept( T(T()) )) {
        assert(_moved < _prev_capacity);
        assert(_moved < _size);
        new (_data + _moved) T(std::move(_prev_data[_moved]));
        _prev_data[_moved].~T();
        ++_moved;
    }

    template<class T>
    void vector_past<T>::try_move() noexcept(noexcept(move_one)) {
        if (_moved < _prev_capacity && _moved < _size) {
            move_one();
        }
    }

    template<class T>
    void vector_past<T>::next_data() {
        assert(_capacity != 0); // container isn't empty
        assert(_capacity == _size); // container is full
        assert(_moved == _prev_capacity); // _prev_data is useless now

        deallocate(_prev_data); // may be nullptr. That's ok.

        _prev_capacity = _capacity;
        _prev_data = _data;

        _capacity *= 2;
        _data = allocate(_capacity);

        _moved = 0;
    }

    template<class T>
    void vector_past<T>::try_expand() {
        if (_size == _capacity) {
            if (_data == nullptr) {
                reallocate(1);
            }
            else {
                next_data();
                // move_one();
            }
        }
    }

    /// construtors
    
    template<class T>
    vector_past<T>::vector_past(const vector_past<T> &other) {
        reserve(other._size);
        while (_size < other._size) {
            new (_data + _size) T(other[_size]);
            ++_size;
        }
    }

    template<class T>
    vector_past<T>::vector_past(vector_past<T> &&other) noexcept: 
        _data(other._data), 
        _prev_data(other._prev_data), 
        _size(other._size), 
        _moved(other._moved),
        _capacity(other._capacity), 
        _prev_capacity(other._prev_capacity) 
    {   
        other._data = nullptr;
        other._prev_data = nullptr;
        other._size = 0;
        other._moved = 0;
        other._capacity = 0;
        other._prev_capacity = 0;
    }

    /// destructor
    
    template<class T>
    vector_past<T>::~vector_past() noexcept {
        resize_down(0);
        reallocate(0);
    }

    /// random_acces

    template<class T>
    T & vector_past<T>::operator[](size_t pos) noexcept {
        assert(_moved <= _prev_capacity);

        if (pos >= _moved && pos < _prev_capacity) {
            return _prev_data[pos];
        } else {
            return _data[pos];
        }
    }

    template<class T>
    const T & vector_past<T>::operator[](size_t pos) const noexcept {
        assert(_moved <= _prev_capacity);

        if (pos >= _moved || pos < _prev_capacity) {
            return _prev_data[pos];
        } else {
            return _data[pos];
        }
    }

    /// resize

    template<class T>
    void vector_past<T>::resize_down(size_t pos) noexcept {
        assert(_moved <= _size);

        size_t i = pos;

        for (; i < _moved; ++i) {
            _data[i].~T();
        }
        for (size_t to = std::min(_prev_capacity, _size); i < to; ++i) {
            _prev_data[i].~T();
        }
        for (; i < _size; ++i) {
            _data[i].~T();
        }

        if (pos < _moved) {
            _moved = pos;
        }
        _size = pos;
    }

    template<class T>
    void vector_past<T>::reallocate(size_t count) {
        T *tmp;

        if (count != 0) {
            tmp = allocate(count);
            size_t i = 0;

            for (; i < _moved; ++i) {
                new (tmp + i) T(std::move(_data[i]));
                _data[i].~T();
            }
            for (size_t to = std::min(_prev_capacity, _size); i < to; ++i) {
                new (tmp + i) T(std::move(_prev_data[i]));
                _prev_data[i].~T();
            }
            for (; i < _size; ++i) {
                new (tmp + i) T(std::move(_data[i]));
                _data[i].~T();
            }
        } else {
            tmp = nullptr;
        }
    
        deallocate(_prev_data); // may be nullptr. Tt's okey
        _prev_data = nullptr;
        _moved = 0;
        _prev_capacity = 0;

        deallocate(_data);
        _data = tmp;
        _capacity = count;
    }

    template<class T>
    void vector_past<T>::reserve(size_t count) {
        if (count > _capacity) {
            reallocate(count);
        }
    }

    template<class T>
    template<class ... args_t>
    void vector_past<T>::resize_emplace(size_t count, const args_t & ... args) {
        if (count < _size) {
            resize_down(count);
        }
        else if (_size < count) {
            reserve(count);
            // std::cerr << _data << ' ' << _capacity << ' ' << _size << endl;
            // std::cerr << _prev_data << ' ' << _prev_capacity << ' ' << _moved << endl;
            while (_size < count) {
                emplace_back(args...);
            }
        }
    }

    template<class T>
    void vector_past<T>::assign(size_t count, const T &init) {
        clear();
        resize(count, init);
    }

    /// swap and operator=

    template<class T>
    void vector_past<T>::swap(vector_past<T> &other) noexcept {
        std::swap(_data, other._data);
        std::swap(_prev_data, other._prev_data);
        std::swap(_size, other._size);
        std::swap(_moved, other._moved);
        std::swap(_capacity, other._capacity);
        std::swap(_prev_capacity, other._prev_capacity);
    }

    template<class T>
    vector_past<T> &vector_past<T>::operator=(vector_past<T> other) {
        swap(other);
        return *this;
    }
    /// emplace_back

    template<class T>
    template<class ... args_t>
    void vector_past<T>::emplace_back(args_t && ... args) {
        try_move();
        try_expand();
        new ((_size < _prev_capacity ? _prev_data : _data) + _size) T(std::forward<args_t>(args)...);
        ++_size;
    }

    /// pop_back

    template<class T>
    void vector_past<T>::pop_back() noexcept {
        assert(_moved <= _size);

        if (_moved == _size) {
            _data[_size - 1].~T();
            --_moved;
        } else if (_prev_capacity < _size) {
            _data[_size - 1].~T();
        } else {
            _prev_data[_size - 1].~T();
        }
        --_size;
    }
}
    
#endif // _VECTOR_PAST_IMPL_HPP_
