

#ifndef _VECTOR_IMPL_HPP_
#define _VECTOR_IMPL_HPP_

namespace my {
	/// construtors

	template<class T>
	vector_common<T>::vector_common(const vector_common &other) {
		reserve(other._size);
		while (_size < other._size) {
			new (_data + _size) T(other[_size]);
			++_size;
		}
	}

	template<class T>
	vector_common<T>::vector_common(vector_common &&other) noexcept: _data(other._data), _size(other._size), _capacity(other._capacity) {
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}

	/// destructor

	template<class T>
	vector_common<T>::~vector_common() noexcept {
		resize_down(0);
		deallocate(_data);
	}

	/// resize

	template<class T>
	void vector_common<T>::resize_down(size_t pos) noexcept {
		while (_size > pos) {
			--_size;
			_data[_size].~T();	
		}
	}

	template<class T>
	void vector_common<T>::reallocate(size_t count) {
		T *tmp;
		if (count != 0) {
			tmp = allocate(count);
			for (size_t i = 0; i < _size; ++i) {
				new (tmp + i) T(std::move(_data[i]));
				_data[i].~T();
			}
		} else {
			tmp = nullptr;
		}

		deallocate(_data);
		_data = tmp;

		_capacity = count;
	}

	template<class T>
	template<class ... args_t>
	void vector_common<T>::resize_emplace(size_t count, const args_t & ... args) {
		if (count < _size) {
			resize_down(count);
		}
		else if (_size < count) {
			reserve(count);
			while (_size < count) {
				emplace_back(args...);
			}
		}
	}

	template<class T>
	void vector_common<T>::assign(size_t count, const T &init) {
		clear();
		resize(count, init);
	}

	/// swap and operator=

	template<class T>
	void vector_common<T>::swap(vector_common &other) noexcept {
		std::swap(_data, other._data);
		std::swap(_size, other._size);
		std::swap(_capacity, other._capacity);
	}

	template<class T>
	vector_common<T> &vector_common<T>::operator=(vector_common other) {
		swap(other);
		return *this;
	}

	/// emplace_back
	
	template<class T>
	void vector_common<T>::try_expand() {
		if (_size == _capacity) {
			reserve(2 * _size + (_size == 0));
		}
	}

	template<class T>
	template<class ... args_t>
	void vector_common<T>::emplace_back(args_t && ... args) {
		try_expand();
		new (_data + _size) T(std::forward<args_t>(args)...);
		++_size;
	}

	/// pop_back

	template<class T>
	void vector_common<T>::pop_back() noexcept {
		--_size;
		_data[_size].~T();
	}
}

#endif // _VECTOR_IMPL_HPP_
