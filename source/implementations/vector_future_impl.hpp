

#ifndef _VECTOR_FUTURE_IMPL_HPP_
#define _VECTOR_FUTURE_IMPL_HPP_

namespace my {
	/// assist

	template<class T>
	void vector_future<T>::move_one() noexcept(noexcept( T(T()) )) {
		new (_next_data + _moved) T(std::move(_data[_moved]));
		_data[_moved].~T();
		++_moved;
	}

	template<class T>
	void vector_future<T>::try_move() noexcept(noexcept( move_one )) {
		if (_moved < _size) {
			move_one();
		}
	}

	template<class T>
	void vector_future<T>::swap_data() {
		deallocate(_data);

		_capacity = next_capacity();
		_data = _next_data;

		_next_data = allocate(next_capacity());
		_moved = 0;
	}

	template<class T>
	void vector_future<T>::try_expand() {
		if (_size == _capacity) {
			assert(_moved == _size);
			if (_data == nullptr) {
				reallocate(1);
			}
			else {
				swap_data();
			}
		}
	}

	/// construtors
	
	template<class T>
	vector_future<T>::vector_future(const vector_future<T> &other) {
		reserve(other._size);
		while (_size < other._size) {
			new (_next_data + _size) T(other[_size]);
			++_size;
		}
		_moved = _size;
	}

	template<class T>
	vector_future<T>::vector_future(vector_future<T> &&other) noexcept : 
	_data(other._data), _next_data(other._next_data), _size(other._size), _moved(other._moved), _capacity(other._capacity) {
		other._data = nullptr;
		other._next_data = nullptr;
		other._capacity = 0;
		other._size = 0;
		other._moved = 0;
	}

	/// destructor
	
	template<class T>
	vector_future<T>::~vector_future() noexcept {
		resize_down(0);
		reallocate(0);
	}

	/// random_acces

	template<class T>
	T & vector_future<T>::operator[](size_t pos) noexcept {
		if (pos < _moved) {
			return _next_data[pos];
		} else {
			return _data[pos];
		}
	}

	template<class T>
	const T & vector_future<T>::operator[](size_t pos) const noexcept {
		if (pos < _moved) {
			return _next_data[pos];
		} else {
			return _data[pos];
		}
	}

	/// resize

	template<class T>
	void vector_future<T>::resize_down(size_t pos) noexcept {
		size_t i = pos;

		while (i < _moved) {
			_next_data[i].~T();
			++i;
		}
		while (i < _size) {
			_data[i].~T();
			++i;
		}

		if (pos < _moved) {
			_moved = pos;
		}
		_size = pos;
	}

	template<class T>
	void vector_future<T>::reallocate(size_t count) {
		T *tmp, *next_tmp;

		if (count != 0) {
			tmp = allocate(count);
			next_tmp = allocate(2 * count);

			for (size_t i = 0; i < _moved; ++i) {
				new (next_tmp + i) T(std::move(_next_data[i]));
				_next_data[i].~T();
			}
			for (size_t i = _moved; i < _size; ++i) {
				new (next_tmp + i) T(std::move(_data[i]));
				_data[i].~T();
			}
		} else {
			tmp = nullptr;
			next_tmp = nullptr;
		}

		_moved = _size;

		deallocate(_next_data);
		_next_data = next_tmp;

		deallocate(_data);
		_data = tmp;
		_capacity = count;
	}

	template<class T>
	void vector_future<T>::reserve(size_t count) {
		if (count > _capacity) {
			reallocate(count);
			if (count <= next_capacity()) { // optimization for small reserves
				while (_moved < _size) {
					move_one();
				}
				swap_data();
			} else {
				reallocate(count);
			}
		}
	}

	template<class T>
	template<class ... args_t>
	void vector_future<T>::resize_emplace(size_t count, const args_t & ... args) {
		if (count < _size) {
			resize_down(count);
		}
		else if (_size < count) {
			reserve(count);
			while (_size < count) {
				emplace_back(args...);
			}
		}
	}

	template<class T>
	void vector_future<T>::assign(size_t count, const T &init) {
		clear();
		resize(count, init);
	}

	/// swap and operator=

	template<class T>
	void vector_future<T>::swap(vector_future<T> &other) noexcept {
		std::swap(_data, other._data);
		std::swap(_next_data, other._next_data);
		std::swap(_size, other._size);
		std::swap(_moved, other._moved);
		std::swap(_capacity, other._capacity);
	}

	template<class T>
	vector_future<T> &vector_future<T>::operator=(vector_future<T> other) {
		swap(other);
		return *this;
	}
	/// emplace_back

	template<class T>
	template<class ... args_t>
	void vector_future<T>::emplace_back(args_t && ... args) {
		try_move();
		try_expand();
		// std::cerr << _data << ' ' << _next_data << ' ' << std::endl;
		// std::cerr << _moved << ' ' << _size  << ' ' << _capacity << std::endl;
		new (_data + _size) T(std::forward<args_t>(args)...);
		++_size;
		try_move();
	}

	/// pop_back

	template<class T>
	void vector_future<T>::pop_back() noexcept {
		if (_moved < _size) {
			_data[_size - 1].~T();
		} else {
			assert(_moved == _size);
			_next_data[_size - 1].~T();
			--_moved;
		}
		--_size;
	}
}
	
#endif // _VECTOR_FUTURE_IMPL_HPP_

