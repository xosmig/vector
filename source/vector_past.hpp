
/**
_prev_data:     ______xxxxxxxxxxxxx
_data:          xxxxxx_____________xxxxxxxxxxxxx_________
                ^     ^            ^            ^        ^
                0   _moved   _prev_capacity   _size   _capacity
where:  x - object 
        _ - empty memory
*/

#ifndef _VECTOR_PAST_HPP_
#define _VECTOR_PAST_HPP_

#include "../defines.hpp"

#include <utility>
#include <memory>

namespace my {

    template<class T>
    class vector_past {
    private:
        static T *allocate(size_t count) noexcept(false) { return static_cast<T*>(operator new(count * sizeof(T))); }
        static void deallocate(T *ptr) noexcept(false) { operator delete(ptr); }

        template<class Val>
        class _iterator : public std::iterator<std::random_access_iterator_tag, Val> {
            typedef std::pair<Val*, Val*> border;
            friend class vector_past;

            Val *p;
            border border1, border2;

            explicit _iterator(Val *_p, const border &_border1, const border &_border2) noexcept : 
                p(_p), border1(_border1), border2(_border2) {}

            void check_border(border &b) {
                if (p == b.first) {
                    p = b.second;
                    --b.first, --b.second; 
                    std::swap(b.first, b.second); // in order to be able to go back
                }
            }
        public:
            _iterator(const _iterator &other) noexcept : p(other.p), border1(other.border1), border2(other.border2) {}
            bool operator==(_iterator &other) const noexcept { return p == other.p; }
            bool operator!=(_iterator &other) const noexcept { return p != other.p; }
            Val &operator*() const noexcept { return *p; }
            Val *operator->() const noexcept { return p; }
            _iterator &operator++() { 
                ++p;
                check_border(border1);
                check_border(border2);
                return *this;
            }
        };

        T *_data = nullptr, *_prev_data = nullptr;
        size_t _size = 0, _moved = 0, _capacity = 0, _prev_capacity = 0;

    protected:
        void resize_down(size_t pos) noexcept; // count <= _size. Destructs element from pos to size - 1 // O(_size - pos)

        // depends on a move constructor
        void move_one() noexcept(noexcept( T(T()) )); // O(1)

        void try_move() noexcept(noexcept( move_one )); // O(1)
        void next_data() noexcept(false); // Allocates new memory. // O(1)

        void try_expand() noexcept(false); // check if it's time to expand

        void reallocate(size_t count) noexcept(false); // makes _capacity = count // O(_size)
    public:
        typedef _iterator<T> iterator;
        // typedef _iterator<const T> const_iterator;

        iterator begin() noexcept {
            size_t to = std::min(_prev_capacity, _size);
            return iterator(
                _data, 
                typename iterator::border(_data + _moved, _prev_data + _moved), 
                typename iterator::border(_prev_data + to, _data + to)
            );
        }
        
        iterator end() noexcept { 
            return iterator(
                _data + _size,
                typename iterator::border(_data + _prev_capacity - 1, _prev_data + _prev_capacity - 1),
                typename iterator::border(_prev_data + _moved - 1, _data + _moved - 1)
            );
        }

        // iterator begin() noexcept {
        //     return iterator(
        //         _data, 
        //         typename iterator::border(nullptr, nullptr), 
        //         typename iterator::border(nullptr, nullptr)
        //     );
        // }
        
        // iterator end() noexcept { 
        //     return iterator(
        //         _data, 
        //         typename iterator::border(nullptr, nullptr), 
        //         typename iterator::border(nullptr, nullptr)
        //     );
        // }

        explicit vector_past(size_t count = 0) noexcept(false) { resize(count); } // O(count)
        explicit vector_past(size_t count, const T &init) noexcept(false) { resize(count, init); } // O(count)
        vector_past(const vector_past<T> &other) noexcept(false); // O(other._size)
        vector_past(vector_past<T> &&other) noexcept; // O(1)

        ~vector_past() noexcept; // O(_size)

        size_t size() const noexcept { return _size; }
        size_t capacity() const noexcept { return _capacity; } // number of elements can be pushed before new allocate of memory

        T & operator[](size_t pos) noexcept; // O(1)
        const T & operator[](size_t pos) const noexcept; // O(1)

        template<class ... args_t>
        void emplace_back(args_t && ... args) noexcept(false); // O(1)
        void push_back(const T &x) noexcept(false) { emplace_back(x); } // O(1)
        void push_back(T &&x) noexcept(false) { emplace_back(std::forward<T>(x)); } // O(1)

        void pop_back() noexcept; // O(1)

        void swap(vector_past<T> &other) noexcept; // O(1)
        vector_past<T> &operator=(vector_past<T> other) noexcept(false); // O(_size) + (copy: O(other._size), move: O(1))

        void reserve(size_t count) noexcept(false); // O(_size + _count)
        void shrink_to_fit() noexcept(false) { reallocate(_size); } // O(_size)
        void clear() noexcept { resize_down(0); } // O(_size)

        template<class ... args_t>
        void resize_emplace(size_t count, const args_t & ... args) noexcept(false); //  O(count + _size)
        void resize(size_t count) noexcept(false) { resize_emplace(count); } // O(count + _size)
        void resize(size_t count, const T &init) noexcept(false) { resize_emplace(count, init); } // O(count + _size)
        void assign(size_t count, const T &init) noexcept(false); // O(count + _size)
    };
}

#include "implementations/vector_past_impl.hpp"

#endif // _VECTOR_PAST_HPP_
