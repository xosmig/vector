
STD = c++1z
DEBUG_FLAGS = -Wall -std=$(STD) -D _GLIBCXX_DEBUG -g -Wextra -Wshadow -Wpedantic -Wfatal-errors

BUILD_OUT = g++ $^ -o $@
BUILD_O = g++ -O1 -c -Wfatal-errors -std=$(STD) $< -o $@
BUILD_HPP = touch $@

TEST_OUT = bin/test.out
TEST_CPP_FILES = test/main.cpp test/speaker_base.cpp test/ta_base.cpp
TEST_O_FILES = bin/main.o bin/speaker_base.o bin/ta_base.o

GEN_OUT = bin/generator.out
GEN_CPP_FILES = test/generator/main.cpp test/ta_base.cpp


.PHONY:    release exec run    generator_debug generator_exec generator_run    clean edit print_logs delete_logs clean_all   default

default: release

#GENERAL:

clean_bin:
	find \( -name "*.out" -or -name "*.o" -or -name "*.tmp" \) -and -delete

clean_tests:
	find \( -name "*.out" -or -name "*.o" -or -name "*.tmp" -or -name "*.test" -or -name "*.list" -or -name "*.ans" \) -and -delete

clean_wo_logs: clean_bin clean_tests

clean_logs:
	find \( -name "*.log" \) -and -delete

clean_all: clean_wo_logs clean_logs

edit:
	subl --project vector.sublime-project

print_logs:
	for name in *.log; do echo $$name; echo; cat $$name; echo; echo; done

#TEST:

debug:
	g++ $(DEBUG_FLAGS) -O1 $(TEST_CPP_FILES) -o $(TEST_OUT)

start_debug:
	gdb ./$(TEST_OUT)

release: $(TEST_OUT)

exec:
	./$(TEST_OUT)

run: debug exec

#GENERATOR:

generator_debug:
	g++ $(DEBUG_FLAGS) -O3 $(GEN_CPP_FILES) -o $(GEN_OUT)

generator_exec:
	./$(GEN_OUT)

generator_run: generator_debug generator_exec

#FILES:

$(TEST_OUT): $(TEST_O_FILES)
	$(BUILD_OUT)

bin/main.o: test/main.cpp source/vector_common.hpp source/vector_future.hpp source/vector_past.hpp test/test_manual.hpp test/test_auto.hpp
	$(BUILD_O)



source/vector_common.hpp: source/implementations/vector_common_impl.hpp
	$(BUILD_HPP)

source/vector_future.hpp: source/implementations/vector_future_impl.hpp defines.hpp
	$(BUILD_HPP)

source/vector_past.hpp: source/implementations/vector_past_impl.hpp defines.hpp
	$(BUILD_HPP)


test/test_manual.hpp: test/speaker_full.hpp test/speaker_ender.hpp defines.hpp
	$(BUILD_HPP)

test/speaker_full.hpp: test/speaker_base.hpp
	$(BUILD_HPP)

test/speaker_ender.hpp: test/speaker_base.hpp
	$(BUILD_HPP)

test/test_auto.hpp: test/counter.hpp test/ta_base.hpp
	$(BUILD_HPP)

test/counter.hpp: defines.hpp
	$(BUILD_HPP)


bin/ta_base.o: test/ta_base.cpp test/ta_base.hpp
	$(BUILD_O)


bin/speaker_base.o: test/speaker_base.cpp test/speaker_base.hpp
	$(BUILD_O)


bin/ta_base.hpp: defines.hpp
	$(BUILD_HPP)
